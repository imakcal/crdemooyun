﻿using Assets.Scripts.Classes.Gameplay;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelCamera))]
public class LevelCameraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelCamera levelCamera = (LevelCamera)target;

        if (GUILayout.Button("Reposition Camera"))
        {
            levelCamera.CreateInstance();
        }
    }
}