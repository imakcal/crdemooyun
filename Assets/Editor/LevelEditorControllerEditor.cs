﻿using Assets.Scripts.Constants;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof(LevelEditorController))]
    public class LevelEditorControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            LevelEditorController levelEditorController = (LevelEditorController)target;

            foreach (var pipe in levelEditorController.PipesList) // Adds an add button for every installment in the levelEditorController.PipesList.
            {
                if (GUILayout.Button("Add " + pipe.name))
                {
                    var newPipe = PrefabUtility.InstantiatePrefab(pipe) as GameObject;
                    newPipe.transform.SetParent(levelEditorController.PlayableObjectsGroup.transform);
                }
            }

            if (GUILayout.Button("Save Level")) // Adds a save button.
            {
                var sceneEditorPath = EditorSceneManager.GetActiveScene().path;
                string[] newScenePath = sceneEditorPath.Split(char.Parse("/"));
                newScenePath[newScenePath.Length - 1] = levelEditorController.LevelName + "_LVL.unity";

                if (levelEditorController.LevelName == "") // Check if the level name given.
                {
                    Debug.LogError(GameDictionary.EnterLevelName);
                    return;
                }

                if (System.IO.File.Exists(string.Join("/", newScenePath))) // Check if a level with same name exists.
                {
                    Debug.LogError(GameDictionary.PathAlreadyExists);
                    return;
                }

                var newScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive); // Create a new scene.

                // Add the objects to the new scene.
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.LevelController, levelEditorController.LevelController.transform.position, levelEditorController.LevelController.transform.rotation), newScene);
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.Canvas, levelEditorController.Canvas.transform.position, levelEditorController.Canvas.transform.rotation), newScene);
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.EventSystem, levelEditorController.EventSystem.transform.position, levelEditorController.EventSystem.transform.rotation), newScene);
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.GroundGroup, levelEditorController.GroundGroup.transform.position, levelEditorController.GroundGroup.transform.rotation), newScene);
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.PlayableObjectsGroup, levelEditorController.PlayableObjectsGroup.transform.position, levelEditorController.PlayableObjectsGroup.transform.rotation), newScene);
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.LevelCamera, levelEditorController.LevelCamera.transform.position, levelEditorController.LevelCamera.transform.rotation), newScene);
                EditorSceneManager.MoveGameObjectToScene(Instantiate(levelEditorController.LevelLight, levelEditorController.LevelLight.transform.position, levelEditorController.LevelLight.transform.rotation), newScene);

                bool saveOk = EditorSceneManager.SaveScene(newScene, string.Join("/", newScenePath)); // Save the new scene.
                Debug.Log("Saved: " + saveOk); // Inform level editors.
                EditorSceneManager.CloseScene(newScene,true); // Close the new scene.
            }
        }
    }
}