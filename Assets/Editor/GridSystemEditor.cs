﻿using Assets.Scripts.Classes.Gameplay;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GridSystem))]
public class GridSystemEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GridSystem gridSystem = (GridSystem)target;

        if (GUILayout.Button("Add Grid"))
        {
            gridSystem.CreateGrid();
        }
    }
}