﻿using Assets.Scripts.Classes.Gameplay;
using UnityEngine;

namespace Assets.Scripts.Classes.EditorExecutables
{
    [ExecuteInEditMode]
    public class CameraAligner : MonoBehaviour // Camera aligner repositions the camera inside the Level Editor scene.
    {
        float _cameraViewAngle; // Vertical field of view of the camera.
        float _groundAreaHalfDiagonalDistance; // Half of the diagonal length of the grid.

        private void Update()
        {
            if (GridSystem.Instance != null && LevelCamera.Instance != null)
            {
                var gridInstance = GridSystem.Instance;

                _groundAreaHalfDiagonalDistance = Mathf.Sqrt(Mathf.Pow(gridInstance.NumOfCellsInHorizontal * gridInstance.NodeDiameter, 2) + Mathf.Pow(gridInstance.NumOfCellsInVertical * gridInstance.NodeDiameter, 2)) / 2;
                _cameraViewAngle = GetComponent<Camera>().fieldOfView;
                transform.position = new Vector3(gridInstance.NumOfCellsInHorizontal * gridInstance.NodeDiameter / 2 + LevelCamera.Instance.additionalDistInXAxis, 0 + LevelCamera.Instance.additionalDistInYAxis, gridInstance.NumOfCellsInVertical * gridInstance.NodeDiameter / 2 + LevelCamera.Instance.additionalDistInZAxis); // Positions the camera to the middle of the grid.
                transform.position -= LevelCamera.Instance.ZoomLevel * transform.forward * (_groundAreaHalfDiagonalDistance / Mathf.Tan(_cameraViewAngle * Mathf.Deg2Rad)); // Positions the camera with a distance to the center of the grid.
            }
        }
    }
}
