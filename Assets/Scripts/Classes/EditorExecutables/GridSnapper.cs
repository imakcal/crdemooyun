﻿using Assets.Scripts.Classes.Gameplay;
using UnityEngine;

namespace Assets.Scripts.Classes.EditorExecutables
{
    [ExecuteInEditMode]
    public class GridSnapper : MonoBehaviour // Grid snapper is used to snap the added installments inside the Level Editor to the grid. Works after the "Add Grid" button is pressed due to the instance requirement.
    {
        private void Update()
        {
            if (GridSystem.Instance != null)
            {
                transform.position = new Vector3(Mathf.Floor(gameObject.transform.position.x) + GridSystem.Instance.NodeRadius, 0, Mathf.Floor(gameObject.transform.position.z) + GridSystem.Instance.NodeRadius);
            }
        }
    }
}
