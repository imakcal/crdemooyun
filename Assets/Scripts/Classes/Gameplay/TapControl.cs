﻿using Assets.Scripts.Classes.Gameplay;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Classes.UI
{
    public class TapControl : MonoBehaviour
    {
        public static TapControl Instance;
        public bool GameStarted = true;
        [SerializeField] private LayerMask _virtualLayerMask;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            LevelController.gameStarted += ActivateTouching;
        }

        public void ActivateTouching()
        {
            GameStarted = true;
        }

        private void Update()
        {
            if (Input.anyKey) // Check for an input.
            {
                if (GameStarted) // Check if the game has started.
                {
                    
                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        return;
                    }

                    if (Input.GetMouseButtonDown(0)/*Input.GetTouch(0).phase == TouchPhase.Began*/) // While taking a build for android or ios, mouse input must be replaced with touch.
                    {
                        RaycastHit hit = new RaycastHit();
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition/*Input.GetTouch(0).position*/); // While taking a build for android or ios, mouse input must be replaced with touch.
                        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _virtualLayerMask))
                        {
                            hit.collider.gameObject.GetComponent<VirtualsControl>().StartRotating();
                        }
                    }
                }
            }
        }
    }
}
