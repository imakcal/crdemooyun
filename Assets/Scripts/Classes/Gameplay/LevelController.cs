﻿using Assets.Scripts.Classes.Gameplay;
using System;
using Assets.Scripts.Classes.UI;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;
    public static event Action gameOver;
    public static event Action gameStarted;
    public GameObject startGameButton;
    public GameObject nextLevelButton;
    public GameObject youWinText;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        //Canvas buttons are losing their references when cloned to the new level. These assignments are
        //calling them back from the hierarchy. Remember that these assignments will cause the play in level
        //editor to give null exception error, since the canvas in the is named "Canvas".
        startGameButton = GameObject.Find("Canvas(Clone)").transform.Find("StartGame_Btn").gameObject;
        nextLevelButton = GameObject.Find("Canvas(Clone)").transform.Find("NextLevel_Btn").gameObject;
        youWinText = GameObject.Find("Canvas(Clone)").transform.Find("YouWin_Img").gameObject;

        //These methods are being called due to the same Canvas problem as mentioned above.
        startGameButton.GetComponent<StartGameButton>().AssignButton();
        nextLevelButton.GetComponent<NextLevelButton>().AssignButton();

        WaterDestinationControl.Victory += GameOver;

    }

    public void StartGame()
    {
        GameStarted();
    }

    private void GameOver()
    {
        gameOver?.Invoke();
        nextLevelButton = GameObject.Find("Canvas(Clone)").transform.Find("NextLevel_Btn").gameObject; // Properties becomes null even though Start() function work when the player goes to the next level. This is just some dirty solution for this small problem.
        youWinText = GameObject.Find("Canvas(Clone)").transform.Find("YouWin_Img").gameObject; // Properties becomes null even though Start() function work when the player goes to the next level. This is just some dirty solution for this small problem.
        nextLevelButton.SetActive(true);
        youWinText.SetActive(true);
        Handheld.Vibrate();
    }

    private void GameStarted() // Informing method that the game has started.
    {
        gameStarted?.Invoke();
        startGameButton.SetActive(false);
    }

    public void GoToNextLevel() // Triggered when the next level button is pressed.
    {
        if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        }

        else
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }

}
