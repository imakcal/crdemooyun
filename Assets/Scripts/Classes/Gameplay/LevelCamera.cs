﻿using UnityEngine;

namespace Assets.Scripts.Classes.Gameplay
{
    public class LevelCamera : MonoBehaviour
    {
        public static LevelCamera Instance;
        public float ZoomLevel;
        public float additionalDistInXAxis;
        public float additionalDistInYAxis;
        public float additionalDistInZAxis;

        public void CreateInstance()
        {
            if (Instance == null) // Singleton
            {
                Instance = this;
            }
        }
    }
}