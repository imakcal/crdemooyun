﻿using System;
using UnityEngine;

namespace Assets.Scripts.Classes.Gameplay
{
    public class GridSystem : MonoBehaviour
    {
        public static event Action GridReady;
        public static GridSystem Instance; // Singleton.
        public GameObject GridReferenceGround; // Ground where the grid will take as reference.
        public GameObject GridGround; // Tile the grid will place.
        public int NumOfCellsInHorizontal; // Number of grid squares to be placed on X axis.
        public int NumOfCellsInVertical; // Number of grid squares to be placed on Z axis.
        public float NodeRadius => 0.5f; // Mostly, the _nodeDiameter will be used, but radius will be useful while building the grid. Since 1 Unity unit is taken as base scale, radius is 0.5f.
        private Node[,] _grid; // The grid we will store the nodes inside
        public float NodeDiameter => 2 * NodeRadius;
        public Vector3 BottomLeftPoint => new Vector3(0, 0, 0);

        private void Awake()
        {
            CreateSelfInstance();
        }

        public void CreateGrid() // This method clears the existing ground tiles, adds new ones with the given numbers in horizontal and vertical axes. Then informs the listeners that the grid is ready.
        {
            CreateSelfInstance();
            ClearGridReference();

            _grid = new Node[NumOfCellsInHorizontal, NumOfCellsInVertical]; // Initializing the node array.

            for (var i = 0; i < NumOfCellsInHorizontal; i++) // Grid column count
            {
                for (var j = 0; j < NumOfCellsInVertical; j++) // Grid row count
                {
                    var worldPoint = BottomLeftPoint + new Vector3(NodeRadius, 0, NodeRadius) + new Vector3(i * NodeDiameter, 0, j * NodeDiameter);
                    var newGroundTile = Instantiate(GridGround, worldPoint, Quaternion.identity); // Instantiate the ground sprite at the same position as the node.
                    newGroundTile.transform.SetParent(GridReferenceGround.transform);
                    _grid[i, j] = new Node(false, worldPoint, i, j);
                }
            }
            GridReady?.Invoke();
        }

        public void ClearGridReference() // Deletes all the grid tiles inside the GridReferenceGround.
        {
            while (GridReferenceGround.transform.childCount != 0)
            {
                DestroyImmediate(GridReferenceGround.transform.GetChild(0).gameObject);
            }
        }

        public void CreateSelfInstance() // Creates an instance of this class.
        {
            if (Instance == null) // Singleton
            {
                Instance = this;
            }
        }
    }
}
