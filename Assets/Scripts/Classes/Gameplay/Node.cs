﻿using UnityEngine;

namespace Assets.Scripts.Classes.Gameplay
{
    public class Node // Node just keeps a virtual type for grid. Each tile on grid is represented by a node.
    {
        public Vector2 WorldPosition; // World position of the node.
        public int XPosInGrid; // X position in the node matrix.
        public int YPosInGrid; // Y position in the node matrix.
        public bool isObstructed;

        public Node(bool pIsObstructed, Vector3 pWorldPosition, int pXPosInGrid, int pYPosInGrid)
        {
            isObstructed = pIsObstructed;
            WorldPosition = pWorldPosition;
            XPosInGrid = pXPosInGrid;
            YPosInGrid = pYPosInGrid;
        }
    }
}
