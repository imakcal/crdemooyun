﻿using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelsMenuController : MonoBehaviour
{
    public GameObject LevelListScrollViewContent; // Content of the scrollview which displays the levels.
    public GameObject LevelButton; // Prefab for level buttons.

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    private void Awake()
    {
        LoadLevelList();
    }

    public void LoadLevelList() // Gets all the scenes in build and selects the levels, then creates the buttons.
    {
        var scenes = EditorBuildSettings.scenes;

        foreach (var scene in scenes)
        {
            if (scene.path.EndsWith("_LVL.unity"))
            {
                CreateNewLevelButton(scene);
            }
        }
    }

    public void CreateNewLevelButton(EditorBuildSettingsScene scene) // Creates a button and associates it with the level.
    {
        var newButton = Instantiate(LevelButton); // Create the button,
        var pathWords = scene.path.Split('/'); // Split the path to get the name,

        newButton.transform.Find("LevelName_Txt").GetComponent<TextMeshProUGUI>().text = pathWords[pathWords.Length-1].Split('.')[0].Split('_')[0]; // Assign name to the text area,
        newButton.GetComponent<Button>().onClick.AddListener(() => LoadScene(pathWords[pathWords.Length - 1].Split('.')[0])); // Add listener to navigate to the level,
        newButton.transform.SetParent(LevelListScrollViewContent.transform); // And place the button inside the scrollview.
    }
}
