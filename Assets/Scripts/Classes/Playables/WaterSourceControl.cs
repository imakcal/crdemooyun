﻿using UnityEngine;

public class WaterSourceControl : InstallmentControl
{
    private void Awake()
    {
        pipeRenderer = GetComponent<Renderer>();
        isFilledWithWater = true;
        pipeRenderer.material.color = Color.blue;
    }

    protected override void EmptyWater()
    {
        // Since there will always be water in the source, EmptyWater() in InstallmentControl is overriden here to do nothing.
    }
}
