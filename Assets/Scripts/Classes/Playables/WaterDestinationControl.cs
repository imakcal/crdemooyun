﻿using System;
using UnityEngine;

public class WaterDestinationControl : InstallmentControl
{
    public static event Action Victory; // Event to inform the level controller that the game is over.

    private void Awake()
    {
        pipeRenderer = GetComponent<Renderer>();
        isFilledWithWater = false;
        pipeRenderer.material.color = Color.white;
    }

    protected override void FillWithWater() // Since the water fill in the destination installment needs to invoke victory this method is overriden.
    {
        isFilledWithWater = true;
        pipeRenderer.material.color = Color.blue;
        Victory?.Invoke();
    }

}
