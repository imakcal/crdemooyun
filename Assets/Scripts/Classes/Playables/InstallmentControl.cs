﻿using System.Collections.Generic;
using UnityEngine;

public class InstallmentControl : MonoBehaviour // Main class for every installment on the game area. (Installments refers to pipes, water destination and water source.)
{
    public List<GameObject> waterSupplierList = new List<GameObject>(); // Water suppliers of the installment.
    public List<GameObject> collidingPipesList = new List<GameObject>(); // Neighbour pipes on four sides (up, left, down, right) of the installment.
    public bool isFilledWithWater; // Boolean check parameter to identify whether the installment is filled with water or not.
    public Renderer pipeRenderer; // Renderer of the installment. TODO: pipeRenderer => installmentRenderer

    private void Awake()
    {
        pipeRenderer = GetComponent<Renderer>();
    }

    private void OnTriggerEnter(Collider collider) // Every installment has colliders switched to work as trigger. When two trigger collides, both installment adds each other as neighbour and check for water supply.
    {
        collidingPipesList.Add(collider.gameObject);

        if (collider.GetComponent<InstallmentControl>().isFilledWithWater)
        {
            if (!isFilledWithWater)
            {
                waterSupplierList.Add(collider.gameObject);
                FillWithWater();
            }
        }
    }

    private void OnTriggerExit(Collider collider) // When two colliders stop triggering each other, both installments remove each other from neighbour list and checks if the other installment was a water supplier.
    {
        collidingPipesList.Remove(collider.gameObject);

        if (waterSupplierList.Contains(collider.gameObject))
        {
            waterSupplierList.Remove(collider.gameObject);

            if (waterSupplierList.Count == 0)
            {
                EmptyWater();
            }
        }
    }

    protected virtual void FillWithWater() // This method fills the water inside the installment, then checks the neighbours if they will be filled as well.
    {
        isFilledWithWater = true;
        pipeRenderer.material.color = Color.blue;

        foreach (var collidingPipe in collidingPipesList)
        {
            var collidingPipeScript = collidingPipe.GetComponent<InstallmentControl>();
            
            if (!collidingPipeScript.waterSupplierList.Contains(gameObject) && !waterSupplierList.Contains(collidingPipe)) // To prevent water sources to form a loop, which will cause the water to be stuck event if there is no source connected, an installment cannot be supplier to any of its supplier. All chain of neighbours are checked iteratively.
            {
                collidingPipeScript.waterSupplierList.Add(gameObject);
            }

            if (!collidingPipeScript.isFilledWithWater)
            {
                collidingPipeScript.FillWithWater();
            }
        }
    }

    protected virtual void EmptyWater() // Similar to FillWithWater(), but it empties the water from the installment itself and neighbours.
    {
        isFilledWithWater = false;
        pipeRenderer.material.color = Color.white;

        foreach (var collidingPipe in collidingPipesList)
        {
            var collidingPipeScript = collidingPipe.GetComponent<InstallmentControl>();

            if (collidingPipeScript.waterSupplierList.Contains(gameObject))
            {
                collidingPipeScript.waterSupplierList.Remove(gameObject);

                if (collidingPipeScript.waterSupplierList.Count == 0)
                {
                    collidingPipeScript.EmptyWater();
                }
            }
        }
    }
}
