﻿using Assets.Scripts.Classes.UI;
using System;
using System.Collections;
using UnityEngine;

public class VirtualsControl : MonoBehaviour // The box around the installments are virtual shapes to detect the collision of the ray comes from the tap. This class controls them.
{
    public float rotationSpeed;
    protected bool rotationCoroutineWorking;
    public bool isTemplate;

    public void StartRotating() // On tap, installment starts rotating.
    {
        if (!rotationCoroutineWorking)
        {
            StartCoroutine(RotatePipe());
        }
    }

    private IEnumerator RotatePipe() // Rotates installment over time.
    {
        rotationCoroutineWorking = true;
        var targetRotation = transform.rotation * Quaternion.AngleAxis(90, Vector3.up);
        while (Quaternion.Angle(targetRotation, transform.rotation) > 2)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            yield return null;
        }
        transform.rotation = targetRotation;
        rotationCoroutineWorking = false;
    }
}
