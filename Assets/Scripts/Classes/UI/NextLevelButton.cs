﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Classes.UI
{
    public class NextLevelButton : MonoBehaviour // Buttons lost their listeners when they get instantiated and transferred to another scene in Level Editor. This script ensures that the button will have its listeners back.
    {
        public void AssignButton()
        {
            GetComponent<Button>().onClick.AddListener(() => LevelController.Instance.GoToNextLevel());
        }
    }
}